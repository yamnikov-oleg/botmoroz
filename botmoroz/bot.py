from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.jobstores.memory import MemoryJobStore
from apscheduler.schedulers.blocking import BlockingScheduler
from telegram import Bot

from . import settings


def make_job(bot, message_text):
    def send():
        bot.send_message(chat_id=settings.TARGET_CHAT_ID, text=message_text)

    return send


def start():
    bot = Bot(settings.BOT_TOKEN)
    scheduler = BlockingScheduler(
        jobstores={"default": MemoryJobStore()},
        executors={"default": ThreadPoolExecutor(1)},
    )
    for job_time, message_text in settings.SCHEDULE:
        scheduler.add_job(make_job(bot, message_text), "date", run_date=job_time)
    scheduler.start()
